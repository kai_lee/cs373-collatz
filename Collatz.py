#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

precomp = {}

# ------------
# collatz_read
# ------------

def collatz_read (s: str) -> List[int] :
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]
    
    
def cycle_length(x: int):
    n = x
    max = 1
    while (n>1):
        if(n<)

# ------------
# collatz_eval
# ------------

def collatz_eval (i: int, j: int) -> int :
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    assert (i>0)
    assert(j<suze)
    
    maxLength = 1
    for x in range(i,j):
            check = False
            length = 1
            n = x
            while n != 1 and not check:
                if n in precomp.keys():
                    check = True
                    length = precomp[n]
                elif(n % 2 == 0):
                    n = n//2
                    length += 1
                else: 
                    n = 3*n +1
                    length += 1
            if(length > maxLength):
                maxLength = length
            precomp.update(n = length)
        # print(str(s[0]) + " " + str(s[1]) + " " + str(maxLength))
    return maxLength

# -------------
# collatz_print
# -------------

def collatz_print (w: IO[str], i: int, j: int, v: int) -> None :
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve (r: IO[str], w: IO[str]) -> None :
    """
    r a reader
    w a writer
    """
    for s in r :
        i, j = collatz_read(s)
        v    = collatz_eval(i, j)
        collatz_print(w, i, j, v)
